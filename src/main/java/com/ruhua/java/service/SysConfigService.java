
package com.ruhua.java.service;

import com.ruhua.java.dao.SysConfig;
import com.ruhua.java.repository.SysConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SysConfigService {


    @Autowired
    private SysConfigRepository sysConfigRepository;


    public List<SysConfig> getWxPayParam() {

        ArrayList<String> keys = new ArrayList<>();
        keys.add("wx_app_id");
        keys.add("pay_num");
        keys.add("pay_key");
        System.out.println("dddd1");
        List<SysConfig> list = sysConfigRepository.findByKeyIn(keys);
        System.out.println("dddd222");
        return list;
    }


}

