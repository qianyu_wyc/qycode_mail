package com.ruhua.java.service;


import com.ruhua.java.dao.Banner;


public interface BannerService {
    Banner getByName(String name);
    Banner getById(Long id);
}
