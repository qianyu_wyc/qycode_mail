 
package com.ruhua.java.service;

import com.github.wxpay.sdk.*; 
import com.ruhua.java.core.LocalUser;
import com.ruhua.java.dao.Order;
import com.ruhua.java.dao.SysConfig;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.repository.OrderRepository;
import com.ruhua.java.util.CommonUtil;
import com.ruhua.java.util.HttpRequestProxy;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class WxPaymentService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private SysConfigService sysConfigService;

    @Value("${wx.order.pay-callback-host}")
    private String payCallbackHost;

    @Value("${wx.order.pay-callback-path}")
    private String payCallbackPath;



    public Map<String, String> preOrder(Long oid) {
        Long uid = LocalUser.getUser().getId();
        Optional<Order> optionalOrder = this.orderRepository.findFirstByUserIdAndOrderId(uid, oid);
        Order order = optionalOrder.orElseThrow(
                () -> new NotFoundException(50009));

        if (order.needCancel()) {
            throw new NotFoundException(50010);
        }

        WXPay wxPay = this.assembleWxPayConfig();
        Map<String, String> params = this.makePreOrderParams(order.getOrderMoney(), order.getOrderNum());
        Map<String, String> wxOrder;
        try {
            wxOrder = wxPay.unifiedOrder(params);} catch (Exception e) {
            throw new NotFoundException(9999);
        }
        if (this.unifiedOrderSuccess(wxOrder)) {
            this.orderService.updateOrderPrepayId(order.getOrderId(), wxOrder.get("prepay_id"));
        }
        return this.makePaySignature(wxOrder);
    }

    private boolean unifiedOrderSuccess(Map<String, String> wxOrder) {
        if (!wxOrder.get("return_code").equals("SUCCESS")
                || !wxOrder.get("result_code").equals("SUCCESS")) {
            throw new NotFoundException(10007);
        }
        return true;
    }



    private Map<String, String> makePreOrderParams(BigDecimal serverFinalPrice, String orderNo) {
        String payCallbackUrl = this.payCallbackHost + this.payCallbackPath;
        Map<String, String> data = new HashMap<>();
        data.put("body", "Sleeve");
        data.put("out_trade_no", orderNo);
        data.put("device_info", "Sleeve");
        data.put("fee_type", "CNY");
        data.put("trade_type", "JSAPI");

        data.put("total_fee", CommonUtil.yuanToFenPlainString(serverFinalPrice));
        data.put("openid", LocalUser.getUser().getOpenid());
        data.put("spbill_create_ip", HttpRequestProxy.getRemoteRealIp());

        data.put("notify_url", payCallbackUrl);
        return data;
    }



    private Map<String, String> makePaySignature(Map<String, String> wxOrder) {
        List<SysConfig> list=sysConfigService.getWxPayParam();
        QyWxPayConfig qyWxPayConfig = new QyWxPayConfig(list);

        Map<String, String> wxPayMap = new HashMap<>();
        String packages = "prepay_id=" + wxOrder.get("prepay_id");

        wxPayMap.put("appId", qyWxPayConfig.getAppID());

        wxPayMap.put("timeStamp", CommonUtil.timestamp10());
        wxPayMap.put("nonceStr", RandomStringUtils.randomAlphanumeric(32));
        wxPayMap.put("package", packages);
        wxPayMap.put("signType", "HMAC-SHA256");

        String sign;
        try {
            sign = WXPayUtil.generateSignature(wxPayMap, qyWxPayConfig.getKey(), WXPayConstants.SignType.HMACSHA256);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NotFoundException(9991);
        }

        Map<String, String> miniPayParams = new HashMap<>();

        miniPayParams.put("paySign", sign);
        miniPayParams.putAll(wxPayMap);
        miniPayParams.remove("appId");
        return miniPayParams;
    }

    public WXPay assembleWxPayConfig() {
        List<SysConfig> list=sysConfigService.getWxPayParam();
        QyWxPayConfig qyWxPayConfig = new QyWxPayConfig(list);
        WXPay wxPay;
        try {
            wxPay = new WXPay(qyWxPayConfig);
        } catch (Exception ex) {
            throw new NotFoundException(9992);
        }
        return wxPay;
    }

}
