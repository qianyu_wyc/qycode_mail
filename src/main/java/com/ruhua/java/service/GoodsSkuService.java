
package com.ruhua.java.service;

import com.ruhua.java.dao.GoodsSku;
import com.ruhua.java.repository.GoodsSkuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsSkuService {

    @Autowired
    private GoodsSkuRepository goodsSkuRepository;

    public List<GoodsSku> getSkuListByIds(List<Long> ids) {
        return this.goodsSkuRepository.findAllBySkuIdIn(ids);
    }
}
