package com.ruhua.java.service;


import com.ruhua.java.dao.Banner;
import com.ruhua.java.repository.BannerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BannerServiceImpl implements BannerService{
    @Autowired
    private BannerRepository bannerRepository;


    public Banner getByName(String name){
        System.out.println("impl");
        return bannerRepository.findOneByName(name);
    }

    public Banner getById(Long num){
        return bannerRepository.findOneById(num);
    }

}
