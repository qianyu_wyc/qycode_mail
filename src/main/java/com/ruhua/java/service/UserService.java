
package com.ruhua.java.service;

import com.ruhua.java.core.LocalAdmin;
import com.ruhua.java.dao.Article;
import com.ruhua.java.dao.Order;
import com.ruhua.java.dao.User;
import com.ruhua.java.dao.UserAddress;
import com.ruhua.java.dto.AddressDTO;
import com.ruhua.java.dto.ArticleDTO;
import com.ruhua.java.exception.ErrException;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.repository.UserAddressRepository;
import com.ruhua.java.repository.UserRepository;
import com.ruhua.java.util.CommonUtil;
import com.ruhua.java.util.OrderUtil;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserAddressRepository userAddressRepository;


    public User getUserById(Long id) {
        Optional<User> ouser=userRepository.findById(id);
        return ouser.orElseThrow(()->new ErrException("用户不存在"));
    }
    public void updateUserWxInfo(Long uid,Map<String, String> wxUser) {
        User user =this.getUserById(uid);
        user.setNickname(wxUser.get("nickName"));
        user.setHeadpic(wxUser.get("avatarUrl"));
        userRepository.save(user);
    }


    public void upAddress(AddressDTO dto,Long uid) {
        Optional<UserAddress> Aitem = userAddressRepository.findByIdAndUserId(dto.getId(),uid);
        UserAddress item=Aitem.orElseThrow(()->new ErrException("地址不存在"));
        item.setName(dto.getName());
        item.setMobile(dto.getMobile());
        item.setProvince(dto.getProvince());
        item.setDetail(dto.getDetail());
        item.setCity(dto.getCity());
        item.setIsDefault(dto.getIsDefault());
        userAddressRepository.save(item);
    }


    public Long addAddress(AddressDTO dto,Long uid) {
        Long IsDefault=0l;
        Optional opitem=userAddressRepository.findByUserIdDefault(uid);
        if(!opitem.isPresent()){
            IsDefault=1l;
        }
        UserAddress item = UserAddress.builder()
                .name(dto.getName())
                .mobile(dto.getMobile())
                .province(dto.getProvince())
                .detail(dto.getDetail())
                .city(dto.getCity())
                .userId(uid)
                .isDefault(IsDefault)
                .build();
        userAddressRepository.save(item);return item.getId();
    }

    public void setAddress(Long id,Long uid) {

        try {
            Optional<UserAddress> Ditem = userAddressRepository.findByUserIdDefault(uid);
            UserAddress ditem=Ditem.orElse(null);
            if(ditem != null) {
                ditem.setIsDefault(0l);
                userAddressRepository.save(ditem);
            }

            Optional<UserAddress> Aitem = userAddressRepository.findByIdAndUserId(id,uid);
            UserAddress aitem=Aitem.orElseThrow(()->new ErrException("地址不存在"));
            aitem.setIsDefault(1l);
            userAddressRepository.save(aitem);
        }catch (Exception e) {
            throw new NotFoundException(10001);
        }
    }

    public void delAddress(Long id,Long uid) {
        Optional<UserAddress> Aitem = userAddressRepository.findByIdAndUserId(id,uid);
        UserAddress aitem=Aitem.orElseThrow(()->new ErrException("地址不存在"));
        userAddressRepository.delete(aitem);
    }

    public User createDevUser(Long uid) {
        User newUser = User.builder().ucid(uid).build();
        userRepository.save(newUser);
        return newUser;
    }

    public User getUserByucid(Long uuid) {
        return userRepository.findByucid(uuid);
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public void delItem(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (Exception e) {
            throw new NotFoundException(10001);
        }
    }


}

