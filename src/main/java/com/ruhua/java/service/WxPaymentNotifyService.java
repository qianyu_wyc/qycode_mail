 
package com.ruhua.java.service;

import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayUtil;
import com.ruhua.java.core.enumeration.OrderStatus;
import com.ruhua.java.dao.Order;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Optional;

@Service
public class WxPaymentNotifyService {

    @Autowired
    private WxPaymentService wxPaymentService;

    @Autowired
    private OrderRepository orderRepository;

    @Transactional
    public void processPayNotify(String data) {
        Map<String, String> dataMap;
        try {
            dataMap = WXPayUtil.xmlToMap(data);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NotFoundException(9999);
        }

        WXPay wxPay = this.wxPaymentService.assembleWxPayConfig();
        boolean valid;
        try {
            valid = wxPay.isResponseSignatureValid(dataMap);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NotFoundException(9999);

        }
        if (!valid) {
            throw new NotFoundException(9999);
        }

        String returnCode = dataMap.get("return_code");
        String orderNo = dataMap.get("out_trade_no");

        System.out.println("订单编号："+ orderNo);
        String resultCode = dataMap.get("result_code");

        if (!returnCode.equals("SUCCESS")) {
            throw new NotFoundException(9999);
        }
        if (!resultCode.equals("SUCCESS")) {
            throw new NotFoundException(9999);
        }
        if (orderNo == null) {
            throw new NotFoundException(9999);
        }
        this.deal(orderNo);
    }

    private void deal(String orderNo) {
        Optional<Order> orderOptional = this.orderRepository.findFirstByOrderNum(orderNo);
        Order order = orderOptional.orElseThrow(() -> new NotFoundException(9999));

        int res = -1;
        if (order.getState().equals(OrderStatus.UNPAID.value())
                || order.getState().equals(OrderStatus.CANCELED.value())) {
            res = this.orderRepository.updateStatusByOrderNum(orderNo, OrderStatus.PAID.value());
        }
        if (res != 1) {
            throw new NotFoundException(9999);
        }
    }
}
