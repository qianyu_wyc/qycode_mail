package com.ruhua.java.api.cms;

import com.ruhua.java.core.interceptors.ScopeLevel;
import com.ruhua.java.dao.*;
import com.ruhua.java.dto.SysConfigDTO;
import com.ruhua.java.dto.ZhglDTO;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.exception.TrowException;
import com.ruhua.java.repository.ImageRepository;
import com.ruhua.java.repository.KeysRepository;
import com.ruhua.java.repository.SysConfigRepository;
import com.ruhua.java.repository.ZhglRepository;
import com.ruhua.java.service.AdminService;
import com.ruhua.java.service.GoodsService;
import com.ruhua.java.service.TokenService;
import com.ruhua.java.service.UploadService;
import com.ruhua.java.vo.ResVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.ruhua.java.util.CommonUtil.toJsonString;


@RestController
public class CommonController {

    @Autowired
    TokenService tokenService;

    @Autowired
    AdminService adminService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    SysConfigRepository sysConfigRepository;

    @Autowired
    KeysRepository keysRepository;

    @Autowired
    ZhglRepository zhglRepository;

    @Autowired
    private UploadService uploadService;

    @PostMapping({"/admin/up_img","img_category/admin/upload/img"})
    public ResVO<String> uploadImage(@RequestParam("img") MultipartFile file) {
        String url = uploadService.uploadImage(file);
        return ResVO.go(url);
    }

    @GetMapping({
            "order/admin/get_tui_all","rate/admin/get_all_rate",
            "/statistic/admin/get_index_data","video/admin/get_all_video",
            "level/admin/get_all_level","user/admin/get_one_user",
            "reduction/get_reduction_goods","coupon/user/get_coupon","article/getPersonArtical"})
    public ResVO<String> get_index_data(){
        return ResVO.go();
    }



    @PostMapping("/search/admin/add_record")
    public ResVO<String> add_record(@RequestBody Map<String,String> person){
        String name = person.get("name");
        Integer num = Integer.parseInt(person.get("num"));
        if(name == null || num == null){
            throw new TrowException("参数错误");
        }
        List<Keys> keysList=keysRepository.findByName(name);
        if(keysList.size()>0) {
            throw new TrowException("关键词已存在");
        }
        Keys item = Keys.builder()
                .name(name)
                .num(num)
                .build();
        keysRepository.save(item);
        return ResVO.go();
    }


    @DeleteMapping("/search/admin/del_record")
    @Transactional
    public ResVO<String> add_record(@RequestParam Long id){
        System.out.println("id:"+id);
        try {
            keysRepository.delById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new NotFoundException(10001);
        }
        return ResVO.go();
    }


    @GetMapping("/search/record")
    public ResVO<List> all_record(){
        List<Keys> list=keysRepository.findAll();
        return ResVO.go(list);
    }




    @PostMapping("/index/admin/add_zz")
    public ResVO<String> add_zz(@RequestBody Map<String,String> person){
        String name = person.get("bz");
        String zh= person.get("zh_num");
        if(name == null || zh == null){
            throw new TrowException("参数错误");
        }
        Optional<Zhgl> opItem=zhglRepository.findByZhNum(zh);
        System.out.println(toJsonString(opItem));
        if(opItem.isPresent()) {
            throw new TrowException("名称已存在");
        }
        Zhgl item = Zhgl.builder()
                .bz(name)
                .zhNum(zh)
                .build();
        zhglRepository.save(item);
        return ResVO.go();
    }


    @PostMapping("/index/admin/edit_zz")
    @Transactional
    public ResVO<String> edit_zz(@RequestBody ZhglDTO dto) {
        try{
            zhglRepository.upZhgl(dto.getId(), dto.getZhNum(), dto.getBz());
        }catch (Throwable e) {
            System.out.println("更新失败"+e.getMessage());
        }
        return ResVO.go();
    }


    @Transactional
    @DeleteMapping("/index/admin/del_zz")
    @ScopeLevel(10)
    public ResVO<String> ael_zz(@RequestParam Long id){
        System.out.println("id:"+id);
        try {
            zhglRepository.deleteById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new NotFoundException(10001);
        }
        return ResVO.go();
    }

    @GetMapping("/img_category/admin/get_all_img")
    public ResVO<List> get_all_img(){
        List<Image> list=imageRepository.findAll();
        return ResVO.go(list);
    }

    @PutMapping("/order/admin/update")
    public ResVO<String> upstate(@RequestBody Map<String,Object> person){
        String name=person.get("db").toString();
        if(!"goods".equals(name)){
            throw new TrowException("类型错误");
        }
        String field=person.get("field").toString();
        String sid=person.get("id").toString();
        Long id=Long.parseLong(sid);
        goodsService.UpGoodFiled(id,field);
        return ResVO.go();
    }

    @GetMapping("/cms/get_config")
    public ResVO<List> sys_config(@RequestParam Long type){
        System.out.println("type"+type);
        List<SysConfig> list=sysConfigRepository.findByType(type);
        return ResVO.go(list);
    }

    @PostMapping("/cms/edit_config")
    @Transactional
    public ResVO<String> edit_config(@RequestBody List<SysConfigDTO> list){
        try{
            for(SysConfigDTO x:list){
                this.edit_one_config(x);
            }
        }catch (Throwable e) {
            System.out.println("更新配置失败"+e.getMessage());
        }
        return ResVO.go();
    }

    @PostMapping("/cms/edit_one_config")
    @Transactional
    public ResVO<String> edit_one_config(@RequestBody SysConfigDTO dto){
        try{
            Long id=dto.getId();
            String value=dto.getValue();
            sysConfigRepository.upValueById(id,value);
            }catch (Throwable e) {
            System.out.println("更新配置失败"+e.getMessage());
        }
        return ResVO.go();
    }


}
