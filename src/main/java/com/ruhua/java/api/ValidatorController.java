package com.ruhua.java.api;

import com.ruhua.java.dto.PersonDTO;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;



@RestController
@Validated
@RequestMapping("/validator/")
public class ValidatorController {


    @GetMapping("test/{id}")
    public String main2(@PathVariable @Range(min = 1,max = 10) Integer id,
                        @RequestParam @Length(min = 4) String name){
        System.out.println(id+name);
        return "a:"+id+name;
    }


    @GetMapping("body")
    public PersonDTO main3(@RequestBody @Validated PersonDTO person){
        return person;
    }




}
