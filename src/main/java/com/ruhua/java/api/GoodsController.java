package com.ruhua.java.api;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.ruhua.java.bo.PageCounter;
import com.ruhua.java.dao.Goods;
import com.ruhua.java.dao.Rate;
import com.ruhua.java.dto.GoodsDTO;
import com.ruhua.java.dto.SuccessDTO;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.exception.TrowException;
import com.ruhua.java.repository.GoodsRepository;
import com.ruhua.java.repository.RateRepository;
import com.ruhua.java.service.GoodsService;
import com.ruhua.java.util.CommonUtil;
import com.ruhua.java.vo.GoodsVO;
import com.ruhua.java.vo.PagingDozer;
import com.ruhua.java.vo.ResVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/product")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @Autowired
    GoodsRepository goodsRepository;

    @Autowired
    RateRepository rateRepository;


    @PostMapping("/admin/add_product")
    @Transactional
    public ResVO<String> add_product(@RequestBody @Validated GoodsDTO goodsDTO){
        try {
            Long id=goodsService.createItem(goodsDTO);
            goodsService.create_json_sku(id,goodsDTO.getSku());
            goodsService.create_sku(id,goodsDTO.getSku(),goodsDTO.getGoodsName());
        }catch (Throwable e) {
            throw new TrowException("创建失败");
        }
        return ResVO.go();
    }


    @PostMapping("/admin/edit_product")
    public SuccessDTO up_article(@RequestBody @Validated GoodsDTO goodsDTO){
        goodsService.updateItem(goodsDTO);
        return new SuccessDTO();
    }



    @GetMapping("/get_product")
    public ResVO<GoodsVO> goods_one(@RequestParam Long id){
        Goods goods=goodsService.getById(id);
        if(goods == null){
            throw new NotFoundException(10001);
        }
        GoodsVO vo = new GoodsVO();
        BeanUtils.copyProperties(goods, vo);  return ResVO.go(vo);
    }

    @GetMapping({"/admin/all_goods_info","/admin/all_goods_name"})
    public ResVO<List> all_goods_info(){
        return this.goods_all_up();
    }



    @GetMapping("/admin/get_product")
    public ResVO<List> goods_all_up(){
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();List<Goods> goodsList=goodsRepository.findAllByState(1l);
        List<GoodsVO> vos = new ArrayList<>();  goodsList.forEach(s->{
            GoodsVO vo = mapper.map(s,GoodsVO.class);
            vos.add(vo);
        });
        return ResVO.go(vos);
    }


    @GetMapping("/search")
    public ResVO<List> search_pros(@RequestParam String name) {
        List<String> nameList = Arrays.asList(name.split(","));
        List<Goods> goodsList= goodsRepository.findByNames(nameList);
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();List<GoodsVO> vos = new ArrayList<>();  goodsList.forEach(s->{
            GoodsVO vo = mapper.map(s,GoodsVO.class);
            vos.add(vo);
        });
        return ResVO.go(vos);
    }


    @GetMapping("/goods_name")
    public List<Goods> goods_names(@RequestParam String name) {
        List<String> nameList = Arrays.asList(name.split(","));
        List<Goods> list = goodsService.findByNames(nameList);
        return list;
    }


    @GetMapping("/get_cate_pros")
    public ResVO<List> get_cate_pros(@RequestParam Long id) {
        List<Goods> goodsList= goodsRepository.findByCid(id);
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();List<GoodsVO> vos = new ArrayList<>();  goodsList.forEach(s->{
            GoodsVO vo = mapper.map(s,GoodsVO.class);
            vos.add(vo);
        });
        return ResVO.go(vos);
    }


    @GetMapping("/get_recent")
    public ResVO<List> get_recent(@RequestParam String type){
        List<Goods> goodsList= new ArrayList<Goods>();
        switch (type) {
            case "recommend":
                goodsList=goodsRepository.findByisRecommend();
                break;
            case "new":
                goodsList=goodsRepository.findByisNew();
                break;
            case "hot":
                goodsList=goodsRepository.findByisHot();
                break;
            default:
                throw new NotFoundException(10001);
        }
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();List<GoodsVO> vos = new ArrayList<>();  goodsList.forEach(s->{
            GoodsVO vo = new GoodsVO();
            BeanUtils.copyProperties(s, vo);
            vos.add(vo);
        });
        return ResVO.go(vos);
    }



    @GetMapping("/admin/get_products_down")
    public ResVO<List> goods_all_down(){
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();List<Goods> goodsList=goodsRepository.findAllByState(0l);
        List<GoodsVO> vos = new ArrayList<>();  goodsList.forEach(s->{
            GoodsVO vo = mapper.map(s,GoodsVO.class);
            vos.add(vo);
        });
        return ResVO.go(vos);
    }


    @GetMapping("/goods_all_pages")
    public PagingDozer<Goods, GoodsVO> goods_all_pages(@RequestParam(defaultValue = "0") Integer start,
                                                       @RequestParam(defaultValue = "10") Integer count){
        PageCounter pageCounter = CommonUtil.convertToPageParameter(start, count);
        Page<Goods> page = this.goodsService.getGoodsListPages(pageCounter.getPage(), pageCounter.getCount());
        return new PagingDozer<>(page, GoodsVO.class);
    }


    @Transactional
    @PostMapping("/admin/set_sort")
    public SuccessDTO setSort(@RequestBody Map<Long,Long> person){
        try{
            for(Map.Entry<Long,Long> entry : person.entrySet()){
                Long k = entry.getKey();
                Long v = entry.getValue();
                goodsRepository.UpSort(k,v);
            }
        }catch (Throwable e) {
            System.out.println("更新排序失败"+e.getMessage());
        }
        return new SuccessDTO();
    }


    @DeleteMapping("/admin/del_product")
    public ResVO<String> del_product(@RequestParam Long id){
        System.out.println("id:"+id);

            goodsRepository.deleteById(id);
        try {
        } catch (Exception e) {
            throw new NotFoundException(10001);
        }
        return ResVO.go();
    }


    @GetMapping("/get_evaluate")
    public ResVO<List> goods_names(@RequestParam Long id) {
        List<Rate> list=rateRepository.findByGoodsId(id);
        return ResVO.go(list);
    }
}
