package com.ruhua.java.api;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.ruhua.java.core.LocalUser;
import com.ruhua.java.core.interceptors.ScopeLevel;
import com.ruhua.java.dao.User;
import com.ruhua.java.dao.UserAddress;
import com.ruhua.java.dto.AddressDTO;
import com.ruhua.java.exception.ErrException;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.repository.UserAddressRepository;
import com.ruhua.java.service.UserService;
import com.ruhua.java.vo.ResVO;
import com.ruhua.java.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserAddressRepository userAddressRepository;

    @GetMapping("/user/admin/get_all")
    public ResVO<List> all_user() {
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();
        List<User> list = userService.findAll();
        List<UserVO> vos = new ArrayList<>();
        list.forEach(s -> {
            UserVO vo = mapper.map(s, UserVO.class);
            vos.add(vo);
        });
        return ResVO.go(vos);
    }

    @DeleteMapping("/user/admin/del_user")
    public ResVO<String> del_user(@RequestParam Long id) {
        userService.delItem(id);
        return ResVO.go();
    }



    @GetMapping("/user/info")
    @ScopeLevel(1)
    public ResVO<UserVO> user_info() {
        Long id = LocalUser.getUser().getId(); User user = this.userService.getUserById(id);
        UserVO vo = new UserVO();
        BeanUtils.copyProperties(user, vo);  vo.setH5("aa");
        vo.setXcx("bb");
        return ResVO.go(vo);
    }


    @PostMapping("/weixin/upinfo")
    @ScopeLevel()
    public ResVO<String> weixin_upinfo(@RequestBody Map<String, String> person) {
        Long uid = LocalUser.getUser().getId(); this.userService.updateUserWxInfo(uid, person);
        return ResVO.go();
    }



    @GetMapping("/address/get_default_address")
    @ScopeLevel(1)
    public ResVO<UserAddress> get_default_address() {
        Long id = LocalUser.getUser().getId(); Optional<UserAddress> item = this.userAddressRepository.findByUserIdDefault(id);
        UserAddress uitem = null;
        if(item.isPresent()){
            uitem = item.get();
        }
        return ResVO.go(uitem);
    }


    @GetMapping("/address/get_all_address")
    @ScopeLevel()
    public ResVO<List> get_all_address() {
        Long id = LocalUser.getUser().getId(); List<UserAddress> list = this.userAddressRepository.findByUserId(id);
        return ResVO.go(list);
    }


    @GetMapping("/address/get_one_address")
    @ScopeLevel()
    public ResVO<UserAddress> get_one_address(@RequestParam Long id) {
        Long uid = LocalUser.getUser().getId(); Optional<UserAddress> item = this.userAddressRepository.findByIdAndUserId(id, uid);
        UserAddress uitem = item.orElseThrow(() -> new NotFoundException(10003));
        return ResVO.go(uitem);
    }


    @PostMapping("/address/edit_address")
    @ScopeLevel()
    public ResVO<String> edit_address(@RequestBody AddressDTO addressDto) {
        Long uid = LocalUser.getUser().getId(); userService.upAddress(addressDto,uid);
        return ResVO.go();
    }


    @PostMapping("/address/add_address")
    @ScopeLevel()
    public ResVO<String> add_address(@RequestBody AddressDTO addressDto) {
        Long uid = LocalUser.getUser().getId(); Long aid=userService.addAddress(addressDto,uid);
        return ResVO.go();
    }


    @PostMapping("/address/set_default_address")
    @ScopeLevel()
    public ResVO<String> set_default_address(@RequestBody Map<String,Long> person) {
        Long uid = LocalUser.getUser().getId(); userService.setAddress(person.get("id"),uid);
        return ResVO.go();
    }


    @PutMapping("/address/del_address")
    @ScopeLevel()
    public ResVO<String> del_address(@RequestBody Map<String,Long> person) {
        Long uid = LocalUser.getUser().getId(); userService.delAddress(person.get("id"),uid);
        return ResVO.go();
    }
}
