package com.ruhua.java.api;

import com.ruhua.java.dao.Zhgl;
import com.ruhua.java.repository.ZhglRepository;
import com.ruhua.java.service.ArticleService;
import com.ruhua.java.vo.ResVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {
    @Autowired
    private ZhglRepository zhglRepository;

    @GetMapping({"/index/get_all_zz","/index/admin/get_all_zz"})
    public ResVO<List> get_all_zz() {
        List<Zhgl> list = zhglRepository.findAll();
        return ResVO.go(list);
    }
}
