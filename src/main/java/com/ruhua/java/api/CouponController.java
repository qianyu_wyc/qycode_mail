package com.ruhua.java.api;

import com.ruhua.java.core.LocalUser;
import com.ruhua.java.core.interceptors.ScopeLevel;
import com.ruhua.java.dao.Order;
import com.ruhua.java.dao.UserCoupon;
import com.ruhua.java.repository.UserCouponRepository;
import com.ruhua.java.service.OrderService;
import com.ruhua.java.vo.ResVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class CouponController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserCouponRepository userCouponRepository;

    @PostMapping("/coupon/user/order_coupon")
    @ScopeLevel()
    public ResVO<List> user_order_coupon(@RequestBody Map<String, BigDecimal> person){

        Long uid = LocalUser.getUser().getId(); BigDecimal money=person.get("total_money");
        Long endTime=new Date().getTime()/1000;
        List<UserCoupon> list=userCouponRepository.findByUserId(uid,money,endTime);
        return ResVO.go(list);
    }

}
