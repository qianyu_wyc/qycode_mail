package com.ruhua.java.api;

import com.ruhua.java.core.interceptors.ScopeLevel;
import com.ruhua.java.dao.Banner;
import com.ruhua.java.dao.BannerItem;
import com.ruhua.java.dto.BannerDTO;
import com.ruhua.java.dto.SuccessDTO;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.repository.BannerItemRepository;
import com.ruhua.java.repository.BannerRepository;
import com.ruhua.java.service.BannerItemService;
import com.ruhua.java.service.BannerService;
import com.ruhua.java.vo.ResVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@Slf4j
@RestController
@RequestMapping("/banner")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @Autowired
    private BannerItemService bannerItemService;

    @Autowired
    private BannerRepository bannerRepository;

    @Autowired
    private BannerItemRepository bannerItemRepository;


    @GetMapping("/get_banner_content")
    public ResVO<Optional> banner_content(@RequestParam Long id){
        Optional<BannerItem> item = bannerItemRepository.findById(id);
        item.orElseThrow(()-> new NotFoundException(10003));
        return ResVO.go(item);
    }

    @GetMapping("/admin/banner_all_item")
    public ResVO<List> banner_all_banner() {
        List<BannerItem> list= bannerItemService.getBannerItemList();
        return ResVO.go(list);
    }

    @GetMapping("/get_all_banner")
    public ResVO<List> get_all_banner() {

        List<Banner> list=bannerRepository.findAll();
        return ResVO.go(list);
    }



    @GetMapping("/{id}")
    public Banner get_banner(@PathVariable Long id){
        Banner banner=bannerService.getById(id);
        if(banner == null){
            throw new NotFoundException(10001);
        }
        return banner;
    }

    @GetMapping("/get_banner")
    public ResVO<Banner> get_banner2(@RequestParam Long id){
        Banner banner=bannerService.getById(id);
        if(banner == null){
            throw new NotFoundException(10001);
        }
        return ResVO.go(banner);
    }

    @PostMapping("/admin/add_banner")
    @ScopeLevel(10)
    public ResVO<String> add_banner(@RequestBody @Validated BannerDTO bannerDTO){
        System.out.println("a111");
        bannerItemService.createItem(bannerDTO);
        System.out.println("a222");
        return ResVO.go();
    }

    @PostMapping("/admin/edit_banner")
    @ScopeLevel(10)
    public SuccessDTO up_banner(@RequestBody Map<String,Object> person){
        bannerItemService.updateItem(person);
        return new SuccessDTO();
    }

    @PutMapping("/admin/del_banner")
    @ScopeLevel(10)
    public ResVO<String> del_banner(@RequestParam Long id){ 
        bannerItemService.delItem(id);
        return ResVO.go();
    }


    @Transactional
    @PostMapping("/admin/set_sort")
    public SuccessDTO setSort(@RequestBody Map<Long,Long> person){
        try{
            for(Map.Entry<Long,Long> entry : person.entrySet()){
                Long k = entry.getKey();
                Long v = entry.getValue();
                bannerItemRepository.UpSort(k,v);
            }
        }catch (Throwable e) {
            System.out.println("更新排序失败"+e.getMessage());
        }
        return new SuccessDTO();
    }

}
