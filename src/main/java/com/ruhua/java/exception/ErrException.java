package com.ruhua.java.exception;


public class ErrException extends HttpException{

    public ErrException(String msg){
        this.httpStatusCode=404;
        this.code=10002;
        this.msg=msg;
    }
}
