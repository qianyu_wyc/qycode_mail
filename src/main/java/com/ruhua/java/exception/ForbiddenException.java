package com.ruhua.java.exception;

public class ForbiddenException extends HttpException{
    public ForbiddenException(int code){
        this.code=code;
        this.httpStatusCode=403;
        this.msg="Forbidden";
    }


}
