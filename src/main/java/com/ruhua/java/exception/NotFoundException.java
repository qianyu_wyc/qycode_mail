package com.ruhua.java.exception;



public class NotFoundException extends HttpException{

    public NotFoundException(int code){
        this.httpStatusCode=404;
        this.code=code;
        this.msg="暂无数据";
    }
}
