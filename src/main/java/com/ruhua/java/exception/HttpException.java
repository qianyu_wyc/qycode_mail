package com.ruhua.java.exception;




public class HttpException extends RuntimeException{
    protected Integer code;
    protected Integer httpStatusCode=500;
    protected String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

}
