
package com.ruhua.java.exception;

public class UnAuthenticatedException extends HttpException{
    public UnAuthenticatedException(int code){
        this.code = code;
        this.httpStatusCode = 401;
        this.msg="权限错误";
        if(code==10003){
            this.msg="Authorization不存在token";
        }
    }
}
