package com.ruhua.java.core;

import lombok.NoArgsConstructor;


@NoArgsConstructor
public class UnifyResponse {
    private int status;
    private String msg;
    private String request;
    private String data;

    public int getStatus() {
        return status;
    }
    public String getData() {
        return "";
    }

    public String getMsg() {
        return msg;
    }

    public String getRequest() {
        return request;
    }

    public UnifyResponse(int status, String msg, String request){
        this.status=status;
        this.msg=msg;
        System.out.println("msg:"+msg);
        this.request=request;
    }
}
