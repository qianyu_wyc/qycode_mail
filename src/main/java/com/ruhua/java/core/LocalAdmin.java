
package com.ruhua.java.core;
import com.ruhua.java.dao.Admin;

import java.util.HashMap;
import java.util.Map;


public class LocalAdmin {
    private static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<>();

    public static void set(Admin admin, Integer scope) {
        Map<String, Object> map = new HashMap<>();
        map.put("user", admin);
        map.put("scope", scope);
        LocalAdmin.threadLocal.set(map);
    }

    public static void clear() {
        LocalAdmin.threadLocal.remove();
    }

    public static Admin getUser() {
        Map<String, Object> map = LocalAdmin.threadLocal.get();
        Admin user = (Admin)map.get("user");
        return user;
    }

    public static Integer getScope() {
        Map<String, Object> map = LocalAdmin.threadLocal.get();
        Integer scope = (Integer)map.get("scope");
        return scope;
    }
}
