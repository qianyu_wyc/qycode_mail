package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;



@Getter
@Setter
public class BannerDTO {
    @Length(min = 2, max = 10, message = "名称长度需要在2-10")
    private String keyWord;

    @NotNull(message = "没传图片")
    private Long imgId;

    @NotBlank(message = "未选择类型")
    private String type;

    @NotNull(message = "未选择广告位")
    private Long bannerId;

    private long jumpId;    private long sort=0;
}
