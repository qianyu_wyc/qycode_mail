package com.ruhua.java.dto;

import com.ruhua.java.dao.GoodsSku;
import com.ruhua.java.util.ListAndJson;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Convert;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



@Getter
@Setter
public class GoodsDTO {

    private Long goodsId;
    private ArrayList bannerimgs;

    @Length(min = 2, max = 30, message = "名称长度需要在2-30")
    private String goodsName;

    @NotNull(message = "未选择分类")
    private Long category_id;

    @NotNull(message = "库存不能为0")
    private Long stock;

    @DecimalMin(value="0.00", message = "不在合法范围内" )
    @DecimalMax(value="99999999.99", message = "不在合法范围内")
    private BigDecimal price;

    private BigDecimal marketPrice;
    private BigDecimal vipPrice;

    private long sales;
    private long collects;

    private String img_id;
    private String keywords;
    private String description;
    private String content;

    private long state=1;
    private long sort;

    private List<Map<String,Object>> sku;
    }
