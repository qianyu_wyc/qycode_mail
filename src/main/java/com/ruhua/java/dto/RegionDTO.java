package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class RegionDTO {
    private long id;
    @NotNull(message = "未选择顶级")
    @Min(value = 0, message = "请选择")
    private long pid;

    @Length(min = 2, max = 20, message = "名称长度需要在2-20")
    private String name;
    private String mergerName;
    private long level;
    private long ucid;


}
