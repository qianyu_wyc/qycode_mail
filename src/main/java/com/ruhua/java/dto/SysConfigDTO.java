package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


@Getter
@Setter
public class SysConfigDTO {
    @NotNull(message = "数据错误")
    private Long id;
    private Long ucid;
    private Long type;
    @Length(min = 2, max = 20, message = "key长度需要在2-20")
    private String key;
    private String value;

}
