package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class ArticleDTO {
    @Length(min = 2, max = 10, message = "名称长度需要在2-10")
    private String title;

    private Long image;
    private Long id;

    @NotNull(message = "未选择类型")
    @Min(value = 0, message = "请选择父级分类")
    private Long type;

    private String content;


}
