package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


@Getter
@Setter
public class ZhglDTO {
    @NotNull(message = "参数错误")
    private Long id;

    @Length(min = 1, max = 20, message = "长度需要在1-20")
    private String bz;

    @Length(min = 1, max = 20, message = "长度需要在1-20")
    private String zhNum;


}
