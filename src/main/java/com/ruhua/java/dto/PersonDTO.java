package com.ruhua.java.dto;

import com.ruhua.java.validators.PasswordEqual;
import lombok.*;
import org.hibernate.validator.constraints.Length;




@Getter
@Setter
@PasswordEqual(min=8)
public class PersonDTO {
    @Length(min=2,max=10,message="name长度需要在2-10")
    private String name;
    private  Integer id;

    private String password1;
    private String password2;

}
