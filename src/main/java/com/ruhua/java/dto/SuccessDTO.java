
package com.ruhua.java.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
public class SuccessDTO {
    private Integer status = 200;
    private String msg = "ok";
    private Object data = "";

    public SuccessDTO(String msg){
        this.msg=msg;
    }

}