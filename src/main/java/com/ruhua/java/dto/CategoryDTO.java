package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class CategoryDTO {

    private Long categoryId;

    @Length(min = 2, max = 10, message = "名称长度需要在2-10")
    private String categoryName;

    @NotBlank(message = "未选择图片类型")
    private String categoryPic;

    @NotNull(message = "未选择类型")
    private Long pid;

    private String shortName;

}
