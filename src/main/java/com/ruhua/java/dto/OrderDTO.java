package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;



@Getter
@Setter
public class OrderDTO {


    @DecimalMin(value="0.00", message = "不在合法范围内" )
    @DecimalMax(value="99999999.99", message = "不在合法范围内")
    @NotNull(message = "金额不能为空")
    private BigDecimal totalPrice;private List<SkuInfoDTO> json;
    private Long couponId;
    private String orderFrom;
    private String tableNum;
    private String driveType;
    private String paymentType;
    private String msg;

}
