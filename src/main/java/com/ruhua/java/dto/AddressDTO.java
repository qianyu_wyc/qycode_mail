package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class AddressDTO {

    @Length(min = 2, max = 10, message = "名称长度需要在2-10")
    private String name;

    @NotBlank(message = "未选择类型")
    private String mobile;

    @NotBlank(message = "地区不能为空")
    private String province;

    @Length(min = 2, max = 10, message = "长度需要在2-10")
    private String detail;

    private Long isDefault;

    @NotBlank(message = "街道不能为空")
    private String city;

    private Long id;
}
