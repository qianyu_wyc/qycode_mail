package com.ruhua.java.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


@Setter
@Getter
public class SkuInfoDTO {
    private Long skuId;
    private Integer num;
    private BigDecimal price;


}
