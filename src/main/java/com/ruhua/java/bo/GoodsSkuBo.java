
package com.ruhua.java.bo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class GoodsSkuBo {
    private Integer skuId;
    private Integer goodsId;
    private Integer goodsSkuId;
    private Integer num;
    private BigDecimal price;
}
