package com.ruhua.java.repository;



import com.ruhua.java.dao.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminRepository extends JpaRepository<Admin,Long> {

    Admin findOneById(Long id);

    Optional<Admin> findByUsername(String name);

    @Query("select t from rh_admin t where t.username = :username and t.password=:psw")
    Optional<Admin> findNamePsw(@Param("username") String username, @Param("psw") String psw);
    }


