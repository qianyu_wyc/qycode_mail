package com.ruhua.java.repository;

import com.ruhua.java.dao.Goods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface GoodsRepository extends JpaRepository<Goods,Long> {
    Goods findOneByGoodsId(Long id);


    @Query("select t from rh_goods t where t.goodsName like CONCAT('%',:name,'%') ")
    List<Goods> findByNames(@Param("name") List<String> name);

    List<Goods> findAllByState(Long state);

    @Query("select t from rh_goods t where t.categoryId=:id and t.state=1")
    List<Goods> findByCid(@Param("id") Long id);


    @Query("select t from rh_goods t where t.isRecommend = 1 and t.state=1")
    List<Goods> findByisRecommend();

    @Query("select t from rh_goods t where t.isHot = 1 and t.state=1")
    List<Goods> findByisHot();

    @Query("select t from rh_goods t where t.isNew = 1 and t.state=1")
    List<Goods> findByisNew();

    @Modifying  @Query("update rh_goods o set o.sort=:sort where o.goodsId=:id")
    int UpSort(Long id, Long sort);


}
