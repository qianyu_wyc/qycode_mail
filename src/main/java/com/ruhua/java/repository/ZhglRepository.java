package com.ruhua.java.repository;


import com.ruhua.java.dao.Zhgl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ZhglRepository extends JpaRepository<Zhgl, Long> {
    Optional<Zhgl> findByZhNum(String zh);

    @Modifying
    @Query("update rh_zhgl o set o.zhNum=:zh,o.bz=:bz where o.id=:id")
    int upZhgl(@Param("id") Long id,@Param("zh") String zh,@Param("bz") String bz);
}
