package com.ruhua.java.repository;

import com.ruhua.java.dao.Rate;
import com.ruhua.java.dao.UserCoupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;



@Repository
public interface UserCouponRepository extends JpaRepository<UserCoupon,Long> {
    UserCoupon findOneById(Long id);

    @Query("select t from rh_user_coupon t where t.userId = :uid and t.full < :money and t.endTime > :endTime")
    List<UserCoupon> findByUserId(@Param("uid") Long uid,@Param("money") BigDecimal money,@Param("endTime") Long endTime);

}
