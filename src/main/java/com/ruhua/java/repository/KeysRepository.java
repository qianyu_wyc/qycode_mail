package com.ruhua.java.repository;


import com.ruhua.java.dao.Keys;
import com.ruhua.java.dao.Zhgl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KeysRepository extends JpaRepository<Keys, Long> {

     List<Keys> findByName(String name);

     @Modifying
     @Query("delete from rh_keys o where o.id=:id")
     void delById(@Param("id") Long id);
}
