package com.ruhua.java.repository;

import com.ruhua.java.dao.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {

    Category findOneByCategoryId(Long id);

    Optional<Category> findByShortName(String name);

}


