package com.ruhua.java.repository;


import com.ruhua.java.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByOpenid(String openid);
    User findFirstById(Long id);
    User findByucid(Long uuid);
}
