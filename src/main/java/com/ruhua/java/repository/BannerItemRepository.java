package com.ruhua.java.repository;

import com.ruhua.java.dao.BannerItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;



@Repository
public interface BannerItemRepository extends JpaRepository<BannerItem,Long> {
    BannerItem findOneById(Long id);

    @Modifying  @Query("update rh_banner_item o set o.sort=:sort where o.id=:id")
    int UpSort(Long id, Long sort);
}
