package com.ruhua.java.repository;

import com.ruhua.java.dao.Article;
import com.ruhua.java.dao.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface RateRepository extends JpaRepository<Rate,Long> {
    Rate findOneById(Long id);

    @Query("select t from rh_rate t where t.goodsId = :id")
    List<Rate> findByGoodsId(@Param("id") Long id);
}
