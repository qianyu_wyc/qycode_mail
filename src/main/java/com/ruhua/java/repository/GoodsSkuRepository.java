package com.ruhua.java.repository;

import com.ruhua.java.dao.Goods;
import com.ruhua.java.dao.GoodsSku;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface GoodsSkuRepository extends JpaRepository<GoodsSku,Long> {


    List<GoodsSku> findAllBySkuIdIn(List<Long> ids);



}
