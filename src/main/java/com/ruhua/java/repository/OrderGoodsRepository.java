package com.ruhua.java.repository;

import com.ruhua.java.dao.OrderGoods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface OrderGoodsRepository extends JpaRepository<OrderGoods,Long> {

    @Modifying
    @Query("delete from rh_order_goods o where o.orderId=:id")
    void delByOrderId(Long id);

}
