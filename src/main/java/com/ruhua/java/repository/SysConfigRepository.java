package com.ruhua.java.repository;

import com.ruhua.java.dao.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SysConfigRepository extends JpaRepository<SysConfig,Long> {

    SysConfig findOneById(Long id);

    @Query("select t from rh_sys_config t where t.type = :type")
    List<SysConfig> findByType(@Param("type") Long type);

    @Modifying
    @Query("update rh_sys_config o set o.value=:value where o.id=:id")
    int upValueById(@Param("id") Long id,@Param("value") String value);

    @Query("select t from rh_sys_config t where t.key in (:keys) order by t.id")
    List<SysConfig> findByKeyIn(@Param("keys") List<String> keys);


}


