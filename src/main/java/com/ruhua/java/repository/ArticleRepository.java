package com.ruhua.java.repository;

import com.ruhua.java.dao.Article;
import com.ruhua.java.dao.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface ArticleRepository extends JpaRepository<Article,Long> {
    Article findOneById(Long id);

    @Query("select t from rh_article t where t.type = :type and t.isHidden=0 ")
    List<Article> findByType(@Param("type") Long type);
}
