package com.ruhua.java.repository;


import com.ruhua.java.dao.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface UserAddressRepository extends JpaRepository<UserAddress, Long> {


    @Query("select t from rh_user_address t where t.id=:id and t.userId=:uid")
    Optional<UserAddress> findByIdAndUserId(@Param("id") Long id,@Param("uid") Long uid);

    @Query("select t from rh_user_address t where t.userId=:id and t.isDefault=1")
    Optional<UserAddress> findByUserIdDefault(@Param("id") Long id);

    @Query("select t from rh_user_address t where t.userId=:id")
    List<UserAddress> findByUserId(@Param("id") Long id);

}
