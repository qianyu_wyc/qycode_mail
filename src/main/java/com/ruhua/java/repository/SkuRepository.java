package com.ruhua.java.repository;

import com.ruhua.java.dao.GoodsSku;
import com.ruhua.java.dao.Sku;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SkuRepository extends JpaRepository<Sku,Long> {

    List<Sku> findAllByIdIn(List<Long> ids);




}
