package com.ruhua.java.repository;

import com.ruhua.java.dao.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ImageRepository extends JpaRepository<Image,Long> {
    Image findOneById(Long id);
}
