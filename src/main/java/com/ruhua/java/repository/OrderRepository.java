package com.ruhua.java.repository;

import com.ruhua.java.dao.Goods;
import com.ruhua.java.dao.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {

    Order findOneByOrderId(Long id);
    Optional<Order> findByOrderId(Long id);
    Optional<Order> findByOrderNum(String name);

    @Query("select t from rh_order t where t.userId = :id and t.paymentState = 0 order by t.orderId desc")
    List<Order> findByUidAndNotPay(@Param("id") Long id);

    @Query("select t from rh_order t where t.userId = :id and t.paymentState = 1 order by t.orderId desc")
    List<Order> findByUidAndPay(@Param("id") Long id);

    Optional<Order> findFirstByUserIdAndOrderId(Long uid, Long oid);

    Optional<Order> findFirstByOrderNum(String orderNum);

    @Modifying
    @Query("update rh_order o set o.state=:state where o.orderNum=:orderNum")
    int updateStatusByOrderNum(String orderNum, Integer state);


}
