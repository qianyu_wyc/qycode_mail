package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;

@Entity(name = "rh_admin")
@Setter
@Getter
@AllArgsConstructor @NoArgsConstructor
@Builder
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;
    private long ucid;
    private String username;
    private String password;
    private long groupId;
    private String ip;
    private long state;
    private String description;
    private int loginTime;


}
