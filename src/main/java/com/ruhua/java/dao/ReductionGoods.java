package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "rh_reduction_goods")
@Setter
@Getter
@AllArgsConstructor @NoArgsConstructor
@Builder
public class ReductionGoods {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long reductionId;
    private long goodsId;
    private long ucid;


}
