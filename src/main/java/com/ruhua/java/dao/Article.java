package com.ruhua.java.dao;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Objects;

@Entity(name="rh_article")
@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor  @Builder    @Where(clause = "deleted_at is null")  public class Article extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long type;
    private String title;
    private String summary;
    private String content;
    private long image;
    private long isHidden;
    private String author;
    private int ucid;


}
