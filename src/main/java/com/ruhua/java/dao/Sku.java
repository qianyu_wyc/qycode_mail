package com.ruhua.java.dao;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruhua.java.exception.NotFoundException;
import com.ruhua.java.util.MapAndJson;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "rh_sku")
@Setter
@Getter
@AllArgsConstructor @NoArgsConstructor
@Builder
@SQLDelete(sql = "update rh_sku set deleted_at = CURRENT_TIMESTAMP where id = ?")
@Where(clause = "deleted_at is null")  public class Sku extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private BigDecimal price;
    private BigDecimal discountPrice;
    private String img;
    private String title;
    private String goodsName;
    private String specs;
    private long goodsId;
    private long stock;
    private long categoryId;


    public Map<String,String> getSpecs(){
        Map<String,String> maps = (Map) JSONObject.parse(specs);
        return maps;
    }

}
