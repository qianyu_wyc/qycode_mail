package com.ruhua.java.dao;

import com.ruhua.java.util.ListAndJson;
import com.ruhua.java.util.MapAndJson;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Map;


@Entity(name="rh_goods_sku")
@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor  @Builder    public class GoodsSku {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long skuId;
    private long goodsId;

    @Convert(converter = MapAndJson.class)
    private Map<String,Object> json;




}
