package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;

@Entity(name = "rh_image")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long categoryId;
    private String useName;
    private String url;
    private Integer isVisible;

}
