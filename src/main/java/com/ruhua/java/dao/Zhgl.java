package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;

@Entity(name = "rh_zhgl")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Zhgl {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String zhNum;
    private String bz;
    private long ucid;



}
