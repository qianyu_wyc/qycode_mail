package com.ruhua.java.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity(name="rh_banner")
@Getter
@Setter
public class Banner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private Integer ucid;


@OneToMany(mappedBy = "banner")
     @JsonIgnoreProperties(value = { "banner" })
     private List<BannerItem> items;
}
