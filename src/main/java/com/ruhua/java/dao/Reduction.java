package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "rh_reduction")
@Setter
@Getter
@AllArgsConstructor @NoArgsConstructor
@Builder
public class Reduction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private long statu;
    private double full;
    private Double reduce;
    private Integer startTime;
    private Integer endTime;
    private Integer createdAt;
    private Integer updatedAt;
    private Integer deletedAt;
    private long ucid;


}
