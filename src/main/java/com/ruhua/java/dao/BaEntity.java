package com.ruhua.java.dao;

import com.ruhua.java.util.CommonUtil;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;


@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaEntity {

    @CreatedDate    private Long createdAt;

    public String getCreateTime() {
        CommonUtil commonUtil=new CommonUtil();
        return commonUtil.DateString(createdAt);
    }

    public void setCreateTime() {
        this.createdAt = 123l;
    }

}