package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "rh_region")
@Setter
@Getter
@AllArgsConstructor @NoArgsConstructor
@Builder
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long pid;
    private String name;
    private String mergerName;
    private long level;
    private long ucid;

}
