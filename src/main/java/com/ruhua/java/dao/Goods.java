package com.ruhua.java.dao;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


@Entity(name="rh_goods")
@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor  @Builder    @SQLDelete(sql = "update rh_goods set deleted_at = CURRENT_TIMESTAMP where goods_id = ?")
@Where(clause = "deleted_at is null and stock > 0")  public class Goods extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long goodsId;
    private String goodsName;
    private Long categoryId;
    private long sales;
    private long state;
    private long sort;
    private long stock;
    private long collects;
    private long isRecommend;
    private long isHot;
    private long isNew;
    private BigDecimal price;
    private BigDecimal marketPrice;
    private BigDecimal vipPrice;
    private String imgId;
    private String keywords;
    private String description;
    private String content;


    private String bannerimgs;

    @OneToMany(cascade={CascadeType.PERSIST,CascadeType.REMOVE},mappedBy="goodsId")
    private List<Sku> sku;

    public String getImgId(){
        if(bannerimgs == null){
            return "1";
        }else{
            List<String> lis = Arrays.asList(bannerimgs.split(","));
            if(lis.size()>0) {
                return lis.get(0);
            }else{
                return "2";
            }
        }
    }

}
