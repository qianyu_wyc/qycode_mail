package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;

@Entity(name="rh_rate")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Rate extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long orderId;
    private long goodsId;
    private long rate;
    private String content;
    private long userId;
    private String imgs;
    private String headpic;
    private String nickname;
    private String replyContent;
    private long replyTime;
    private long aid; 
    private String video; 
    private long ucid;

}
