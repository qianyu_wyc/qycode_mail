package com.ruhua.java.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass   public abstract class BaseEntity {

    @Column(insertable=false, updatable=false)
    private Date createdAt; 

    @JsonIgnore
    @Column(insertable=false, updatable=false)
    private Date updatedAt;

    @JsonIgnore
    private Date deletedAt;




}
