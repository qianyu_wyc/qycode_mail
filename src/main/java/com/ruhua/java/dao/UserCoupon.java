package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity(name = "rh_user_coupon")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserCoupon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long userId;
    private long couponId;
    private BigDecimal full;
    private BigDecimal reduce;
    private long endTime;
    private long status;
    private long ucid;

}
