package com.ruhua.java.dao;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;


@Entity(name = "rh_category")
@Setter
@Getter
@AllArgsConstructor @NoArgsConstructor
@Builder
@Where(clause = "deleted_at is null")  public class Category extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long categoryId;
    private String categoryName;
    private String shortName;
    private long pid;
    private long level;
    private long isVisible;
    private Integer sort;
    private String categoryPic;



    }
