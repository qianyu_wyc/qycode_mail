package com.ruhua.java.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ruhua.java.service.GoodsService;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "rh_order_goods")
@Getter
@Setter
@Builder
@NoArgsConstructor  @AllArgsConstructor @SQLDelete(sql = "update rh_order_goods set deleted_at = CURRENT_TIMESTAMP where id = ?")
@Where(clause = "deleted_at is null")  public class OrderGoods extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long orderId;
    private long goodsId;
    private String goodsName;
    private long skuId;
    private String skuName;
    private BigDecimal price;
    private BigDecimal costPrice;
    private Integer number;
    private BigDecimal totalPrice;
    private long state;
    private String pic;
    private String remark;

public BigDecimal getTotal() {
        return totalPrice == null ? this.price : this.totalPrice;
    }

    public OrderGoods(Sku sku, Integer num,Long orderId) {
        this.orderId=orderId;
        this.goodsId=sku.getGoodsId();
        this.goodsName=sku.getGoodsName();
        this.skuId=sku.getId();
        this.skuName=sku.getTitle();
        this.price=sku.getPrice();
        this.pic=pic;
        this.number=num;
        this.state=1;
        BigDecimal num2=new BigDecimal(num.toString());
        this.totalPrice=sku.getPrice().multiply(num2);
    }
}
