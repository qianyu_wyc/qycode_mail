package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "rh_user_address")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String mobile;
    private String province;
    private String city;
    private String detail;
    private Long userId;
    private Long isDefault;

}
