package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;

@Entity(name = "rh_user")
@Getter
@Setter
@Builder
@AllArgsConstructor @NoArgsConstructor  public class User extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nickname;
    private String unionid;
    private String openidGzh;
    private String openidApp;
    private String openid;
    private double money;
    private Long signTime;
    private Long signDay;
    private Long points;
    private Long levelId;
    private Long ucid;
    private String headpic;
    private String mobile;
    private String userName;

}
