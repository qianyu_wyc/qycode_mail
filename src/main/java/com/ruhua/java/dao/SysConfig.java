package com.ruhua.java.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Entity(name="rh_sys_config")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysConfig{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String key;
    private String value;
    private String desc;
    private long type;

    @JsonProperty("switch") private long switchs;
    private String other;
    private long ucid;

}
