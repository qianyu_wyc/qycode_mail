package com.ruhua.java.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ruhua.java.core.enumeration.OrderStatus;
import com.ruhua.java.util.CommonUtil;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity(name = "rh_order")
@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor  @Builder    @SQLDelete(sql = "update rh_order set deleted_at = CURRENT_TIMESTAMP where order_id = ?")
@Where(clause = "deleted_at is null")  public class Order extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long orderId;
    private String orderNum;
    private long userId;
    private Integer state;
    private long shipmentState;
    private long paymentState;
    private long rateId;
    private long couponId;
    private long yzcode;
    private String orderFrom;
    private String paymentType;
    private BigDecimal goodsMoney;
    private BigDecimal reductionMoney;
    private double couponMoney;
    private BigDecimal editMoney;
    private BigDecimal orderMoney;
    private String userIp;
    private String goodsPicture;
    private String message;
    private String receiverName;
    private String receiverMobile;
    private String receiverCity;
    private String receiverAddress;
    private String courierNum;
    private String courier;
    private String remarkOne;
    private String remarkTwo;
    private String driveType;
    private String prepayId;
    private long payTime; 
    private String other;
    private String mobile;
    private String address;
    private long count;
    private String payCate;
    private long ucid;
    private String tableNum;
    private Date expiredTime;

    @OneToMany(cascade={CascadeType.PERSIST,CascadeType.REMOVE},mappedBy="orderId")
    private List<OrderGoods> ordergoods;

    @JsonIgnore
    public OrderStatus getStatusEnum() {
        return OrderStatus.toType(this.state);
    }


    public Boolean needCancel() {
        if (!this.getStatusEnum().equals(OrderStatus.UNPAID)) {
            return true;
        }
        boolean isOutOfDate = CommonUtil.isOutOfDate(this.getExpiredTime());
        if (isOutOfDate) {
            return true;
        }
        return false;
    }

}
