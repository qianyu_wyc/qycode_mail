package com.ruhua.java.dao;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "rh_keys")@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Keys {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private Integer num;

}
