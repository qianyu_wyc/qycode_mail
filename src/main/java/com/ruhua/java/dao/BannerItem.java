package com.ruhua.java.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@Entity(name="rh_banner_item")
@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor  @Builder    public class BannerItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String keyWord;
    private long imgId;
    private String type;
    private long jumpId;
    private long sort;

    @ManyToOne
    @JoinColumn(insertable = false,updatable = false,name="imgId")
    private Image imgs;


    private long bannerId;

    @ManyToOne
    @JoinColumn(insertable = false,updatable = false,name="bannerId")
    @JsonIgnoreProperties(value = { "items" })
    private Banner banner;

}
