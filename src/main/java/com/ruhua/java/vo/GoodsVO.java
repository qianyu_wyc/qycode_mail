
package com.ruhua.java.vo;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class GoodsVO<T> {

    private long goodsId;
    private String bannerimgs;
    private String goodsName;
    private Long categoryId;
    private Long isNew;
    private Long isHot;
    private Long isRecommend;
    private BigDecimal price;
    private String imgId;
    private String description;
    private String content;
    private Date createdAt;
    private Date updatedAt;
    private List<T> sku;  private BigDecimal marketPrice;
    private BigDecimal vipPrice;
    private long sales;
    private long state;
    private long stock;


}

