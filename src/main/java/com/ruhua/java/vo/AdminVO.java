
package com.ruhua.java.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class AdminVO<T> {

    private long id;
    private String username;
    private long groupId;
    private long state;
    private String description;
    private int loginTime;

}

