
package com.ruhua.java.vo;

import com.ruhua.java.dao.OrderGoods;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class OrderListVO<T> {

    private long orderId;
    private String orderNum;
    private long userId;
    private Integer state;
    private long shipmentState;
    private long paymentState;
    private long rateId;
    private String paymentType;
    private BigDecimal goodsMoney;
    private BigDecimal orderMoney;
    private String goodsPicture;
    private long payTime;
    private String other;
    private Date expiredTime;
    private Date createdAt;
    private Date updatedAt;
    private List<OrderGoods> ordergoods;
}

