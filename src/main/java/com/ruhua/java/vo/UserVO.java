
package com.ruhua.java.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserVO<T> {

    private long id;
    private String nickname;
    private double money;
    private Long points;
    private Long levelId;
    private String headpic;
    private String mobile;
    private String userName;
    private String h5;
    private String xcx;

}

