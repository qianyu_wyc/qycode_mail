package com.ruhua.java.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResVO<T> {

    private long status;
    private String msg;
    private T data;

    protected ResVO() {
    }

    protected ResVO(long status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }


    public static <T> ResVO<T> go() {
        return new ResVO<T>(200, "操作成功", null);
    }
    public static <T> ResVO<T> go(String msg) {
        return new ResVO<T>(200, msg, null);
    }
    public static <T> ResVO<T> go(T data) {
        return new ResVO<T>(200, "操作成功", data);
    }
    public static <T> ResVO<T> go(T data, String msg) {
        return new ResVO<T>(200, msg, data);
    }


    public static <T> ResVO<T> fail() {
        return new ResVO<T>(500, "操作失败", null);
    }
    public static <T> ResVO<T> fail(String msg) {
        return new ResVO<T>(500, msg, null);
    }
    public static <T> ResVO<T> fail(T data) {
        return new ResVO<T>(500, "操作失败", data);
    }
    public static <T> ResVO<T> fail(T data, String msg) {
        return new ResVO<T>(500, msg, data);
    }







}

