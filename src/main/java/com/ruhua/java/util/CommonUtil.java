
package com.ruhua.java.util;




import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.ruhua.java.bo.PageCounter;
import org.springframework.util.DigestUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class CommonUtil {

    public static  <T> List<T> toList(String object, Class clazz){
        return JSONArray.parseArray(object, clazz);
    }

    public static  <T>  T  toObject(String object,Class clazz){
        return (T)JSONArray.parseObject(object, clazz);
    }

    public static <T> String toJsonString(List<T> list){
        return JSON.toJSONString(list);
    }

    public static <T> String toJsonString(T object){
        return JSON.toJSONString(object);
    }


    public static PageCounter convertToPageParameter(Integer start, Integer count) {
        int pageNum = start / count;

        PageCounter pageCounter = PageCounter.builder()
                .page(pageNum)
                .count(count)
                .build();
        return pageCounter;
    }

    public static Integer getRandom(Integer len){
        int rs = (int) ((Math.random() * 9 + 1) * Math.pow(10, len - 1));
        return rs;
    }
    public static String yuanToFenPlainString(BigDecimal p){
        p = p.multiply(new BigDecimal("100"));
        return CommonUtil.toPlain(p);
    }

    public static String toPlain(BigDecimal p){
        return p.stripTrailingZeros().toPlainString();
    }


    public static Calendar addSomeSeconds(Calendar calendar, int seconds) {
        calendar.add(Calendar.SECOND, seconds);
        return calendar;
    }

    public static String timestamp10(){
        Long timestamp13 = Calendar.getInstance().getTimeInMillis();
        String timestamp13Str = timestamp13.toString();
        return timestamp13Str.substring(0, timestamp13Str.length() - 3);
    }

    public static Boolean isInTimeLine(Date date, Date start, Date end) {
        Long time = date.getTime();
        Long startTime = start.getTime();
        Long endTime = end.getTime();
        if (time > startTime && time < endTime) {
            return true;
        }
        return false;
    }

    public static String md5(String password) {
        String md5Password = DigestUtils.md5DigestAsHex(password.getBytes());
        return md5Password;
    }




    public static Boolean isOutOfDate(Date expiredTime) {
        Long now = Calendar.getInstance().getTimeInMillis();
        Long expiredTimeStamp = expiredTime.getTime();
        if(now > expiredTimeStamp){
            return true;
        }
        return false;
    }

    public String DateString(Long intTime){
        if(intTime == null){
            return "";
        }
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date(Long.parseLong(String.valueOf(intTime))));
    }

}