
package com.github.wxpay.sdk;


import com.ruhua.java.dao.SysConfig;

import java.io.InputStream;
import java.util.List;

public class QyWxPayConfig extends WXPayConfig {

    private String appid;
    private String mchid;
    private String key;

    public QyWxPayConfig(List<SysConfig> list){
        list.forEach((e) -> {
            if(e.getKey().equals("wx_app_id")) {
                this.appid = e.getValue();
            }
            if(e.getKey().equals("pay_key")) {
                this.key = e.getValue();
            }
            if(e.getKey().equals("pay_num")) {
                this.mchid = e.getValue();
            }
        });
    }

    public String getAppID() {
        return this.appid;
    }

    public String getMchID() {
        return this.mchid;
    }

    public String getKey() {
        return this.key;
    }

    public InputStream getCertStream() {
        return null;
    }

    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    public IWXPayDomain getWXPayDomain() {
        IWXPayDomain iwxPayDomain = new IWXPayDomain() {
            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }

            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
            }
        };
        return iwxPayDomain;
    }
}
